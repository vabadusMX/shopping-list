var Product = require('../models/product');

var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/shopping', { useNewUrlParser: true });

var products = [
	new Product({
		imagePath: 'https://images-na.ssl-images-amazon.com/images/I/81Jx5v1%2B%2B2L._SX679_.jpg',
		title: 'Consola Nintendo Switch Gris - Edición Estandar',
		description: 'La consola Nintendo Switch está diseñada para acompañarte dondequiera que vayas, transformándose de consola para el hogar a consola portátil en un instante.',
		price: 900,
	}),
	new Product({
		imagePath: 'https://images-na.ssl-images-amazon.com/images/I/51Od2i6KGKL._SX522_.jpg',
		title: 'Consola Playstation Pro 1TB - PlayStation 4 Standard Edition',
		description: 'PS4 Pro te acerca más a tu juego. Intensifica tus experiencias. Enriquece tus aventuras. Deja que PS4 Pro con carga reforzada te enseñe el camino.',
		price: 701,
	}),
];
var done = 0;
products.forEach(product => {
	product.save(function(err, result) {
		done++;
		if (done === products.length) {
			exit();
		}
	});
});

function exit() {
	mongoose.disconnect();
}