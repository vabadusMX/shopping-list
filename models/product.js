var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema ({
	imagePath: {type: String, required: true},
	title: {type: String, required: true},
	description: {type: String, required: true},
	category: {
		type: String, 
		required: true,
		enum: [
			'Celulares y Telefonía',
			'Electrónica y Tecnología',
			'Videojuegos',
			'Hogar y Jardín',
			'Deportes y Ocio',
			'Animales y Mascotas',
			'Ferretería y Autos',
			'Ropa, Calzado y Accesorios',
			'Mamá y Bebé',
			'Salud, Belleza y Cuidado personal',
			'Joyería y Relojes',
			'Juegos y Juguetes',
			'Libros, Revistas y Comics',
			'Películas, Series de TV y Música',
			'Otras categorías'
		]
	},
	price: {type: Number, required: true},
	seller: {type: String, required: true}
});

module.exports = mongoose.model('Product', schema)