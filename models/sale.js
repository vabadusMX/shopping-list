const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema ({
    seller: {type: String, required: true},
    item: {type: Object, required: true},
    totalQty: {type: String, required: true},
    totalPrice: {type: String, required: true},
    date: {type: Date, required: true}
});

module.exports = mongoose.model('Sale', schema)