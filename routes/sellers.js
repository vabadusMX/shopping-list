const express = require('express');
const router = express.Router();
const multer = require('multer');
const csrf = require('csurf');

const Product = require('../models/product');

var csrfProtection = csrf();
router.use(csrfProtection);


router.get('/products/new', function(req, res, next) {
	let messages = req.flash('error');
	res.render('sellers/form-products', {
		title: 'Add New Product',
		firstTitle: 'Add New Product',
		csrfToken: req.csrfToken(),
		messages: messages,
		hasErrors: messages.length > 0
	});
});

router.get('/products', isLoggedIn, function(req, res, next) {
	if (req.user.type === 2) {
		var messages = req.flash('success')[0];
		messages = req.flash('error')[0];
		Product.find({seller: req.user._id }, function (err, products) {
			var productChunks = [];
			var chunkSize = 3;
			for (var i = 0; i < products.length; i += chunkSize) {
				productChunks.push(products.slice(i, i + chunkSize));	
			}
			res.render('sellers/products', {
				title: 'My Products',
				products: productChunks,
				messages: messages,
				noMessages: !messages
			});
		});
	} else {
		res.redirect('/');
	}
	
});

/*
// var storage = multer.diskStorage({
// 		destination: function (req, file, cb) {
// 		cb(null, 'uploads/')
// 	},
// 		filename: function (req, file, cb) {
// 		cb(null, file.fieldname + '-' + Date.now() + '.jpg')
// 	}
// })

// var upload = multer({ storage: storage }).single('product-img');
*/

router.post('/products/add', isLoggedIn, function (req, res) {
	// upload(req, res, function (err) {
	// 	if (err instanceof multer.MulterError) {
	// 	// A Multer error occurred when uploading.
	// 	} else if (err) {
	// 	// An unknown error occurred when uploading.
	// 	}

	// 	res.redirect('/sellers/');
	// });
	req.checkBody('title', 'Invalid Title').notEmpty();
	req.checkBody('description', 'Invalid Description').notEmpty();
	req.checkBody('imgpath', 'Invalid Image Path').notEmpty().isURL();
	req.checkBody('price', 'Invalid Price').notEmpty().isNumeric();
	const errors = req.validationErrors();
	if (errors) {
		let messages = [];
		errors.forEach(function(error) {
			messages.push(error.msg);
		});
		req.flash('error', messages);
		return res.redirect('/sellers/form-products');
	}
	const product = new Product({
		seller: req.user._id,
		imagePath: req.body.imgpath,
		title: req.body.title,
		description: req.body.description,
		category: req.body.category,
		price: req.body.price
	});
	product.save(function(err, result) {
		if (err) {
			req.flash('error', err.message);
			return res.redirect('/sellers/form-products');
		}
		req.flash('success', 'Product successfully added :)');
		res.redirect('/sellers/products');
	});
});

router.get('/remove-product/:id', isLoggedIn, function(req, res) {
	const productId = req.params.id;
	
	Product.findByIdAndRemove({_id: productId}, function(err, data) {
		if (err) {
			req.flash('error', err.message);
			return res.redirect('/sellers/products');
		}
		req.flash('success', 'Product succesfully REMOVED');
		res.redirect('/sellers/products');
	});
});

router.get('/edit-product/:id', isLoggedIn, function(req, res){
	const productId = req.params.id;
	let messages = req.flash('success')[0];
	messages = req.flash('error')[0];
	Product.findById({_id: productId}, function(err, product) {
		if (err) {
			req.flash('error', err.message);
			return res.redirect('/sellers/products');
		}

		res.render('sellers/form-products-edit', {
			title: 'Edit Product',
			firstTitle: 'Edit Product',
			product: product,
			csrfToken: req.csrfToken(),
			messages: messages,
			noMessages: !messages
		});
	});
});

router.post('/edit-product/update/:id', isLoggedIn, function(req, res) {
	const productId = req.params.id;

	Product.findByIdAndUpdate({_id: productId}, req.body, function(err, data) {
		if (err) {
			req.flash('error', err.message);
			return res.redirect('/sellers/products');
		}
		req.flash('success', 'Product succesfully UPDATED');
		res.redirect('/sellers/products');
	});
});

module.exports = router;

function isLoggedIn(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	}
	rqe.session.oldUrl = req.url;
	res.redirect('/sellers/signin');
}