var passport = require('passport');
var User = require('../models/user');
var LocalStrategy = require('passport-local').Strategy;

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

passport.use('local.signup', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true

}, function(req, email, password, done) {
    req.checkBody('fullname', 'Invalid name').notEmpty();
    req.checkBody('email', 'Invalid email').notEmpty().isEmail();
    req.checkBody('password', 'Invalid password').notEmpty().isLength({min:8});
    var errors = req.validationErrors();
    if (errors) {
        var messages = [];
        errors.forEach(function (error) {
           messages.push(error.msg);
        });
        return done(null, false, req.flash('error', messages));
    }
    User.findOne({'email': email}, function (err, user) {
        if(err) {
            return done(err);
        }
        if (user) {
            return done(null, false, {message: 'Email is already in use.'});
        }
        if (password !== req.body.vpassword) {
            return done(null, false, {message: 'The passwords do not match.'});
        }
        const newUser = new User();
        let usertype = req.body.controlselecttype;

        if (usertype === 'Buy') {
            newUser.type = 1;
        } else if (usertype === 'Sell') {
            newUser.type = 2;
        } else {
            return done(null, false, {message: 'Error assigning user type'});
        }
        newUser.fullname = req.body.fullname;
        newUser.email = email;
        newUser.password = newUser.encryptPassword(password);
        
        newUser.save(function (err, result) {
            if (err) {
                return done(err)
            }
            return done(null, newUser);
        })
    });
}));

passport.use('local.signin', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, function (req, email, password, done) {
    req.checkBody('email', 'Invalid email').notEmpty().isEmail();
    req.checkBody('password', 'Invalid password').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        var messages = [];
        errors.forEach(function (error) {
           messages.push(error.msg);
        });
        return done(null, false, req.flash('error', messages));
    }
    User.findOne({'email': email}, function (err, user) {
        if(err) {
            return done(err);
        }
        if (!user || !user.validPassword(password)) {
            return done(null, false, {message: 'No user found or incorrect password'});
        }
        return done(null, user);
    });
}));