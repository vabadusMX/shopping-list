const express = require('express');
const router = express.Router();
const csrf = require('csurf');
const passport = require('passport');

const Order = require('../models/order');
const Sale = require('../models/sale');
const Cart = require('../models/cart');

const csrfProtection = csrf();
router.use(csrfProtection);

router.get('/order', isLoggedIn, function(req, res, next) {
	const messages = req.flash('success')[0];
	if (req.user.type === 1) {
		const cart = new Cart(req.session.cart);
		const products = cart.generateArray();
		req.session.cart = null;
		res.render('user/order', {
			title: 'Order Information',
			order: products,
			total: cart.totalPrice,
			messages: messages,
			noMessages: !messages
		});
	}
});

router.get('/profile', isLoggedIn, function (req, res, next) {
	var messages = req.flash('success')[0];
	messages = req.flash('error')[0];
	if (req.user.type === 2) {
		req.session.usertype = req.user.type;
		Sale.find({seller: req.user._id}, function(err, sales) {
			if (err) {
				return res.write('Error!');
			}
	
			// var cart;
			// sales.forEach(function(sale) {
			// 	cart = new Cart(sale);
			// 	sale.item = cart.generateArray();
			// });
			
			res.render('user/profile', { 
				title: 'My Profile',
				sales: sales, 
				user: req.user.fullname,
				messages: messages,
				noMessages: !messages
			});
		});
	} else if (req.user.type === 1) {
		req.session.usertype = null;
		Order.find({user: req.user}, function(err, orders) {
			if (err) {
				return res.write('Error!');
			}
	
			var cart;
			orders.forEach(function(order) {
				cart = new Cart(order.cart);
				order.items = cart.generateArray();
			});

			res.render('user/profile', { 
				title: 'My Profile',
				orders: orders, 
				user: req.user.fullname,
				messages: messages,
				noMessages: !messages
			});
		});
	}
});

router.get('/logout', function(req, res, next) {
	req.session.usertype = null;
	req.logout();
	res.redirect('/');
});

router.use('/', notLoggedIn, function (req, res, next) {
	next();
});

router.get('/signup', function(req, res, next) {
	var messages = req.flash('error');
	res.render('user/signup', {
		title: 'Sign Up',
		csrfToken: req.csrfToken(), 
		messages: messages, 
		hasErrors:messages.length > 0
	});
});

router.post('/signup', passport.authenticate('local.signup', {
	failureRedirect: '/user/signup',
	failureFlash: true
}), function(req, res, next) {
	if (req.session.oldUrl) {
		let oldUrl = req.session.oldUrl;
		req.session.oldUrl = null;
		res.redirect(oldUrl);
	} else {
		res.redirect('/user/profile');
	}
});

router.get('/signin', function (req, res, next) {
	var messages = req.flash('error');
	res.render('user/signin', {
		title: 'Sign In',
		csrfToken: req.csrfToken(), 
		messages: messages, 
		hasErrors:messages.length > 0
	});
});

router.post('/signin', passport.authenticate('local.signin', {
	failureRedirect: '/user/signin',
	failureFlash: true
}), function(req, res, next) {
	if (req.session.oldUrl) {
		let oldUrl = req.session.oldUrl;
		req.session.oldUrl = null;
		res.redirect(oldUrl);
	} else {
		res.redirect('/user/profile');
	}
});

module.exports = router;

function isLoggedIn(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	}
	res.redirect('/');
}

function notLoggedIn(req, res, next) {
	if (!req.isAuthenticated()) {
		return next();
	}
	res.redirect('/');
}